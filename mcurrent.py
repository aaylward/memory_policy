#!/usr/bin/env python3
#===============================================================================
# mcurrent.py
#===============================================================================

"""Display the current memory policy"""




# Imports ----------------------------------------------------------------------

import argparse

import mutil




# Function ---------------------------------------------------------------------

def main(args):
    policy = mutil.load_policy(mutil.CURRENT_POLICY_PATH)
    mutil.print_policy(policy)


def parse_arguments():
    parser = argparse.ArgumentParser(
        description=('Display the current memory policy'))
    parser.add_argument(
        '-f', '--flat', action='store_true',
        help='print without colors and formatting')
    return parser.parse_args()




# Execute ----------------------------------------------------------------------

if __name__ == '__main__':
    args = parse_arguments()
    main(args)
