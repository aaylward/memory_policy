# memory_policy

Tools for managing memory resources on a CentOS 7 machine. Examples:

Induct a user into the memory policy:
```
python3 minduct.py -u <username>
```

Propose a 32 GB memory reservation:
```
python3 mreserve.py 32
```

Enact a proposed memory policy:
```
sudo python3 menact.py
```

See usage status of unreserved memeory:
```
python3 musage.py -g free
```
